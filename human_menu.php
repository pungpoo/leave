<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml2/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="Author" content="Stu Nicholls" />
<link href="menu/css/reset.css" rel="stylesheet" type="text/css"/>
<!--<link href="menu/css/960.css" rel="stylesheet" type="text/css"/> -->
<link href="menu/css/coolMenu.css" rel="stylesheet" type="text/css" media="screen"/>
<script type="text/javascript" src="menu/js/modernizr-1.6.min.js"></script>

</head>
<body>

<center>
<table width="1024" border="1" cellpadding="0" cellspacing="0" align="center" style="background:#2f8be8" >
<tr>
<td align="center"><img src="image/logo.png"  width="1024" /></td>
</tr>
<tr >
<td bgcolor="#2f8be8"  >
	<div class="container_16" >
		<ul id="coolMenu">
			<!-- <li><a href="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;หน้าหลัก&nbsp;&nbsp;&nbsp;</a></li> -->
			<li><a href="user_profile.php">ข้อมูลส่วนตัว</a></li>
			<li><a href="#"> สถิติการลา </a>
				<ul class="noJS">
				<li type="disc" ><a href="user_home.php?ntime=<?=time();?>" style="width:auto">ลาป่วย ลากิจ</a></li>
				<li type="disc" ><a href="user_home_vc.php?ntime=<?=time();?>" style="width:auto">ลาพักร้อน</a></li>
				<li type="disc" ><a href="user_home_wt.php?ntime=<?=time();?>" style="width:auto">ลาในเวลาปฏิบัติงาน</a></li>
				<li type="disc" ><a href="in_out.php" style="width:auto">สถิตการเข้า-ออก</a></li>
				</ul>
			</li>
			<li><a href="in_out.php" style="width:auto">สถิติการเข้า-ออก</a></li>
            <li><a href="#"> สร้างใบลา </a>
				<ul class="noJS">
				<li type="disc" ><a href="L_form_add.php?ntime=<?=time();?>" style="width:auto">ลาป่วย ลากิจ</a></li>
				<li type="disc" ><a href="vc_form_add.php?ntime=<?=time();?>" style="width:auto">ลาพักร้อน</a></li>
				<li type="disc" ><a href="wt_form_add.php?ntime=<?=time();?>" style="width:auto">สร้างใบขอลาในเวลาปฏิบัติงาน</a></li>
				<li type="disc" ><a href="urt_form_add.php" style="width:auto">แบบรายงานไม่ได้บันทึกเวลา</a></li>
				</ul>
			</li>
            <li><a href="#">อนุมัติผู้ใต้บังคับบัญชา</a>
				<ul class="noJS">
				<li type="disc" ><a href="leader_show.php?ntime=<?=time();?>" style="width:auto">อนุมัติการลาป่วย ลากิจ</a></li>
				<li type="disc" ><a href="leader_show_vc.php?ntime=<?=time();?>" style="width:auto" >อนุมัติการลาพักร้อน</a></li>
				</ul>
			</li>
			<li><a href="#"> ข้อมูลการลา </a>
				<ul class="noJS">
				<li type="disc" ><a href="human_show.php?ntime=<?=time();?>" style="width:auto">ตรวจสอบการลาป่วย ลากิจ</a></li>
				<li type="disc" ><a href="human_show_vc.php?ntime=<?=time();?>" style="width:auto">ตรวจสอบการลาพักร้อน</a></li>
					<li type="disc" ><a href="human_show_wt.php" style="width:auto">ตรวจสอบการลาในเวลาปฏิบัติงาน</a></li>
				<li type="disc" ><a href="human_stat.php?ntime=<?=time();?>" style="width:auto">สถิติการลาป่วย ลากิจ</a></li>
				<li type="disc" ><a href="human_stat_vc.php?ntime=<?=time();?>" style="width:auto">สถิติการลาพักร้อน</a></li>
				<li type="disc"><a href="human_show_urt.php" style="width:auto">สถิติการขอบันทึกเวลาปฏิบัติงาน</a></li>
				<li type="disc" ><a href="report_61.php" style="width:auto">สรุปการลาของบุคลากร</a></li>
                </ul>
			</li>
			<li><a href="#">บุคลากร</a>
				<ul class="noJS">
				<li type="disc" ><a href="add_user.php?ntime=<?=time();?>" style="width:auto">เพิ่มบุคลากร</a></li>
				<li type="disc" ><a href="human_edit.php?ntime=<?=time();?>" style="width:auto">แก้ไขข้อมูลบุคลากร</a></li>
				</ul>
			</li>
            <li><a href="http://www.op.mahidol.ac.th/orpr/newhrsite/Document/GuideBook/13%20สิทธิการลาหยุดงาน.pdf" style="width:auto" target="_blank">ระเบียบการลา</a></li>
			<li><a href="index.php" style="width:auto">ออกจากระบบ</a></li>
        </ul>
	</div>
</td>
</tr>
</table>
</center>
</body>
</html>
