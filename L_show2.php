<? session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LEAVE SYSTEM - ใบลา</title>
<style type="text/css">
p {
	text-align: center;
}
</style>
</head>
<body background="image/wp2.jpg" style="background-size:cover">
<? $id = $_SESSION["id"] ;
require_once('function.php');
require_once('connect_new.php');
include 'thaidate.php';
include 'thaidatecon.php';
include 'head_menu.php';

$date = date('Y/m/d');
$datethai =  ThaiEachDate("$date");

$leave=select("tblleave","where id ='".$_GET["id"]."' ");

$emp=select("tblemp","where emp_id = '$id' ");
$leader=select("tblemp","where emp_id = '".$leave["lf_leader"]."' ");

?>
<center>
<table width="1024" border="1" bordercolor="#000000" align="center" bgcolor="#FFFFFF"><tr><td>
<br /><br />

<center>
<font size="-1">
<table width="700" height="287"  align="center"  cellpadding="4">

  <tr>
    <td align="center"><font size="+2"><b>ข้อมูลการลา</b></font></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right">เลขที่ใบลา : <?= $leave["lf_id"]?></td>
  </tr>
  <tr>
    <td align="right">วัน/เดือน/ปี : <?=$leave["lf_date1"]?></td>
  </tr>
  <tr>
    <td><b>เรียน</b> ผู้อำนวยการสถาบันนวัตกรรมการเรียนรู้ </td>
  </tr>
  <tr>
    <td><font color="#FFFFFF">.</font> </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    ข้าพเจ้า___<u><?=$emp["emp_title"]?><?=$emp["emp_name"]?>&nbsp;<?=$emp["emp_lname"]?></u>_____&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    ตำแหน่ง___<u><?=$emp["emp_position"]?></u>_____
   </td>
  </tr>
  <tr>
    <td> สังกัด___<u><?=$emp["emp_unit"]?></u>_______________</td>
  </tr>
  <tr>
    <td>ขอลา ___<u>
  	<? if ($leave["lf_type"]=='1'){?>
     ป่วย__ 
    <? }
     else if ($leave["lf_type"]=='2'){?>
     กิจส่วนตัว__</u> &nbsp;&nbsp;&nbsp;&nbsp; เนื่องจาก <u>__<?=$leave["lf_reason"]?></u>__
    <? }
    else if ($leave["lf_type"]=='3'){?>
    __<u>คลอดบุตร___
    <? }
    else if ($leave["lf_type"]=='4'){?>
    อุปสมบท___
	<? }?>
    </u>
</td>
  </tr>
  <tr>
    <td>ตั้งแต่วันที่___<u><? echo DateThai($leave["lf_start"]); ?></u>___
    <? 
	if($leave["lf_halfday"]=='1' ){echo "(เต็มวัน)";}
	elseif($leave["lf_halfday"]=='2' ){echo "(ครึ่งวันเช้า)";}
    else if ($leave["lf_halfday"]=='3' ){echo "(ครึ่งวันบ่าย)";}
	?>
     &nbsp;&nbsp;ถึงวันที่ ___<u><? echo DateThai($leave["lf_end"]); ?></u>___
     <? 
	if($leave["lf_halfday2"]=='1' ){echo "(เต็มวัน)";}
	elseif($leave["lf_halfday2"]=='2' ){echo "(ครึ่งวันเช้า)";}
    else if ($leave["lf_halfday2"]=='3' ){echo "(ครึ่งวันบ่าย)";}
	?>
    </td>
  </tr>
   <tr>
    <td>มีกำหนด__<u><?=$leave["lf_day"]?></u>__วัน</td>
  </tr>
    <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>ข้าพเจ้าได้ลา 
    ___<u>
  	<? if ($leave["lf_last_type"]=='1'){?>
     ป่วย 
    <? }
     else if ($leave["lf_last_type"]=='2'){?>
     กิจส่วนตัว
    <? }
    else if ($leave["lf_last_type"]=='3'){?>
     คลอดบุตร
    <? }
    else if ($leave["lf_last_type"]=='4'){?>
    อุปสมบท
	<? }?>
    </u>_____
   	
  </td>
  </tr>
  <tr>
    <td>ครั้งล่าสุดตั้งแต่วันที่___<u><? if($leave["lf_last_start"]){$strDate=$leave["lf_last_start"]; echo DateThai($strDate);}else echo "-" ; ?></u>___ ถึงวันที่ ___<u><? if($leave["lf_last_end"]){$strDate=$leave["lf_last_end"]; echo DateThai($strDate); }else echo "-" ;?></u>___ &nbsp;มีกำหนด__<u><? if($leave["lf_last_day"]){echo $leave["lf_last_day"];}else echo "-" ; ?></u>__วัน</td>
  </tr>
  <tr>
    <td>ในระหว่างลาจะติดต่อข้าพเจ้าได้ที่เบอร์โทรศัพท์ ___<u><?=$emp["emp_tel"]?></u>___ &nbsp;หรือ E-mail ___<u><?=$emp["emp_mail"]?></u>___</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr align="center">
    <td>สถิติการลาในปีงบประมาณนี้</td>
  </tr>
  <tr>
    <td align="center">
    <table width="400" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000">
  <tr align="center" height="50">
    <td width="30%" ><b><br />ประเภทการลา</b></td>
    <td width="20%"><b><br />ลามาแล้ว</b><br />(วัน)</td>
    <td width="20%"><b><br />ลาครั้งนี้</b><br />(วัน)</td> 
    <td width="20%"><b><br />รวมเป็น</b><br />(วัน)</td> 
  </tr>
  
  <tr align="center">
    <td>&nbsp;ลาป่วย</td>
    <td>&nbsp;<? if ($leave["lf_sick1"]){ echo $leave["lf_sick1"]; } else{ echo "-"; } ?></td>
    <td>&nbsp;<? if ($leave["lf_sick2"]){ echo $leave["lf_sick2"]; } else{ echo "-"; } ?></td>
    <td width="20%"><?=$leave["lf_sick1"] + $leave["lf_sick2"]?></td>
  </tr>
  
  <tr align="center">
    <td>&nbsp;ลากิจ</td>   
    <td>&nbsp;<? if ($leave["lf_kit1"]){ echo $leave["lf_kit1"]; } else{ echo "-"; } ?></td>
    <td>&nbsp;<? if ($leave["lf_kit2"]){ echo $leave["lf_kit2"]; } else{ echo "-"; } ?></td>
	<td width="20%"><?=$leave["lf_kit1"] + $leave["lf_kit2"]?></td>
  </tr>
  
  <tr align="center">
    <td>&nbsp;ลาคลอด</td>
    <td>&nbsp;<? if ($leave["lf_son1"]){ echo $leave["lf_son1"]; } else{ echo "-"; } ?></td>
    <td>&nbsp;<? if ($leave["lf_son2"]){ echo $leave["lf_son2"]; } else{ echo "-"; } ?></td>
	<td width="20%"><?=$leave["lf_son1"] + $leave["lf_son2"]?></td>   
  </tr>
  
  <tr align="center">
    <td>&nbsp;ลาอุปสมบท</td>  
    <td>&nbsp;<? if ($leave["lf_monk1"]){ echo $leave["lf_monk1"]; } else{ echo "-"; } ?></td>
     <td>&nbsp;<? if ($leave["lf_monk2"]){ echo $leave["lf_monk2"]; } else{ echo "-"; } ?></td>
	<td width="20%"><?=$leave["lf_monk1"] + $leave["lf_monk2"]?></td> 
  </tr>
</table>
    </td>
  </tr>
   <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>  
	<table width="100%" cellpadding="0" cellspacing="0"  align="center">
  	<tr align="center" height="35">
    	<td >&nbsp;</td> 
    	<td ><input type="radio" name="check" value="<?=$leave["lf_status1"];?>"<? if(trim($leave["lf_status1"])== '1') echo "checked";?> disabled="disabled"> ตรวจสอบแล้ว </td> 
  	</tr>
  
  	<tr align="center" >
   	 <td width="50%" height="30">(ลงชื่อ)___<u><?=$emp["emp_title"]?><?=$emp["emp_name"]?>&nbsp;<?=$emp["emp_lname"]?></u>___</td> 
    	<td width="50%" height="30">(ลงชื่อ)___________________ ผู้ตรวจสอบ</td> 
  	</tr>
  	<tr align="center">
   	 <td >ตำแหน่ง___<u><?=$emp["emp_position"]?></u>___</td> 
   	 <td height="25">ตำแหน่ง __________________</td>
   	</tr>
  	
  	<tr  align="center">
   	 <td>&nbsp;</td>
   	 <td height="25">วันที่__________________</td>
 	 </tr>
	</table>
    </td>
  </tr>
  
  
  <tr>
   <td align="center">**********************************************************************************</td>
   <td>&nbsp;</td>
  </tr>  
    <tr>
    <td>
<table width="100%" cellpadding="0" cellspacing="0"  align="center">
  <tr align="center" >
    <td width="50%" height="30"><b>ความเห็นผู้บังคับบัญชา</b></td> 
    <td width="50%"><b>คำสั่ง</b></td> 
  </tr>
  <tr> 
    <td align="center" height="30"><input type="radio" name="app1" value="1" <? if(trim($leave["lf_status2"])== '1') echo "checked";?> disabled="disabled" />อนุญาต &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="app1" value="0" disabled="disabled" <? if(trim($leave["lf_status2"])== '2') echo "checked";?>/>ไม่อนุญาต</td>
    <td align="center" ><input type="radio" name="app2" value="1" disabled="disabled" <? if(trim($leave["lf_status3"])== '1') echo "checked";?>/>อนุญาต &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="app2" value="0" disabled="disabled" <? if(trim($leave["lf_status3"])== '2') echo "checked";?>/>ไม่อนุญาต</td>
  </tr>  
  <tr align="center">
    <td height="30" align="center">(ลงชื่อ)________________________ ผู้บังคับบัญชา</td> 
    <td >(ลงชื่อ)______________________ผู้ออกคำสั่ง</td> 
   </tr>
    
  <tr  align="center">
    <td height="25">ตำแหน่ง ____________________</td>
    <td>ตำแหน่ง__________________</td>
  </tr>
  <tr  align="center">
    <td  height="25" >วันที่__________________</td>
    <td>วันที่__________________</td>
  </tr>
   <tr align="center">
    <td height="30">&nbsp;</td>     
    <td >&nbsp;</td>
  </tr>
  <tr  align="center">
    <td ><!--<a href="<?=$_SERVER["PHP_SELF"];?>?Action=Update&amp;id=<?=$_GET["data"];?>">ยืนยันการตรวจสอบ</a>--></td>
    <input name="check" type="hidden"  value="True" />
    <input name="idd" type="hidden"  value="<?=$_GET["data"];?>" />
   
  </tr>
 </form> 
</table>  
</table>
</table>
</center>
</center>
</font>
</body>
</html>