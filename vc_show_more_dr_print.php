<? session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="Author" content="Stu Nicholls" />
<link href="menu/css/reset.css" rel="stylesheet" type="text/css"/>
<!--<link href="menu/css/960.css" rel="stylesheet" type="text/css"/> -->
<link href="menu/css/coolMenu.css" rel="stylesheet" type="text/css" media="screen"/>
<script type="text/javascript" src="menu/js/modernizr-1.6.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LEAVE SYSTEM - VACATION</title>
<style type="text/css">
p {
	text-align: center;
}
</style>
</head>

<body  background="image/wp2.jpg" style="background-size:cover">
<? $id = $_SESSION["id"] ;

require_once('function.php');
require_once('connect.php');
include 'thaidate.php';
include 'thaidatecon.php';
include 'timediff.php';
//include 'head_menu.php';
$date = date('Y/m/d');
$datethai =  ThaiEachDate("$date");

$vc=select("tblvacation","where id = '".$_GET["data"]."' ");
$emp=select("tblemp","where emp_id = '".$vc["emp_id"]."' ");
$leader=select("tblemp","where emp_id = '".$vc["vc_leader"]."' ");
$boss=select("tblemp","where emp_id = '".$vc["vc_boss"]."' ");
$vc_start = select("tblvacation"," where emp_id = '$id' AND vc_period = '2560' ORDER BY  `id` ASC LIMIT 1 ");


?>
<center>
<table width="1024" border="1" bordercolor="#000000" align="center" style="background-color:#FFFFFF"><tr><td>
<br /><br />
<center>
<font size="-1">

<table width="700" height="287" cellpadding="5" cellspacing="5" >

  <tr>
    <td align="center"><br /><font size="+2"><b>แบบใบลาพักผ่อน</b></font></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right">เลขที่ใบลา : <?=$vc[vc_id]?></td>
  </tr>
  <tr>
    <td align="right">วัน/เดือน/ปี : <?=$vc[vc_date]?></td>
  </tr>
  <tr>
    <td><b>เรื่อง ขอลาพักผ่อน</b> </td>
  </tr>
  <tr>
    <td><b>เรียน</b> ผู้อำนวยการสถาบันนวัตกรรมการเรียนรู้ </td>
  </tr>
  <tr>
    <td><font color="#FFFFFF">.</font> </td>
  </tr>
  <tr>
    <td>
    ข้าพเจ้า___<u><?=$emp["emp_sex"]?><?=$emp["emp_name"]?>&nbsp;<?=$emp["emp_lname"]?></u>____&nbsp;&nbsp;
    ตำแหน่ง___<u><?=$emp["emp_position"]?></u>________
   </td>
  </tr>
  <tr>
    <td> สังกัด___<u><?=$emp["emp_unit"]?></u>________</td>
  </tr>
  <tr>
    <td>มีสิทธิลาพักผ่อนในปีนี <?=$vc_start[vc_balance]?>  วันทำการ</td>
  </tr>
  <tr>
   	<td>ขอลาพักร้อนตั้งแต่วันที่__<u><?=$vc["vc_start"]?></u>__ถึง__<u><?=$vc["vc_end"]?></u>__  รวมเป็นเวลา __<u><?=$vc["vc_day"]?></u>__ วันทำการ
        </td>
  </tr>
  <!--<tr>
    <td>ขอลาพักผ่อนตั้งแต่วันที่.....<?=Datethai($rowvc["vc_start"])?>..... ถึงวันที่ ..... <?=Datethai($rowvc["vc_end"])?>.....  &nbsp;มีกำหนด...<?=$rowvc["vc_day"]?>...วัน</td>
  </tr>-->
  <tr>
    <td>ในระหว่างลาจะติดต่อข้าพเจ้าได้ที่เบอร์โทรศัพท์ _<u><?=$emp["emp_tel"]?></u>_ &nbsp;หรือ E-mail _<u><?=$emp["emp_mail"]?></u>_</td>
  </tr>
	<tr height="50"><td> <br /><br /><center>สถิติการลาพักผ่อนในปีงบประมาณ 2560<br />	<br />
    <table width="400" border="1" cellpadding="5" cellspacing="5" bordercolor="#000000" align="center">
  <tr align="center" height="20">
    <td>&nbsp;</td>
    <td><b>ลามาแล้ว<br/>(วันทำการ)</b></td>
    <td><b>ลาครั้งนี้<br/>(วันทำการ)</b></td>
     <td><b>รวมเป็น<br/>(วันทำการ)</b></td>

  </tr>
	<tr align="center" height="20">
    <td width=20%>จำนวน (วัน)</td>
    <td width=20%> <?=$vc["vc_totalday"]-$vc["vc_day"]?> </td>
    <td width=20%><?=$vc["vc_day"]?></td>
    <td width=20%><?=$vc["vc_totalday"]?></td>
  </tr>
	</table>
    </center>
    </td></tr>
    <tr height="50"><td>&nbsp;</td></tr>
	<tr>
    <td>
	<table width="100%" cellpadding="0" cellspacing="0"  align="center">
  	<tr align="center" height="35">
    	<td >&nbsp;</td>
    	<td ><input type="radio" name="app1" value="1"<? if(trim($vc["vc_status1"])== '1') echo "checked";?> disabled="disabled"> ตรวจสอบแล้ว </td>
  	</tr>
	<tr align="center" >
   	<td width="50%" height="25">(ลงชื่อ)___<u><?=$emp["emp_title"]?><?=$emp["emp_name"]?>&nbsp;<?=$emp["emp_lname"]?></u>___</td>
    <td height="25">
	<? if($vc[vc_status1]== "0"){?>(ลงชื่อ)___________________________ผู้ตรวจสอบ</td>
	<? } else if($vc[vc_status1] != "0"){?>(ลงชื่อ)___<u>นายเสฏฐวุฒิ อุรา</u>___ผู้ตรวจสอบ </td> <? }?>
  	</tr>
  	<tr align="center">
   	 <td >ตำแหน่ง___<u><?=$emp[emp_position]?></u>___</td>
      <td height="25">
	<? if($vc[vc_status1]== "0"){?> ตำแหน่ง____________________</td>
	<? } else if($vc[vc_status1] != "0"){?> ตำแหน่ง ___<u>นักทรัพยากรบุคคล</u>___</td> <? }?>
    </tr>
  	<tr  align="center">
   	 <td>&nbsp;</td>
   	 <td height="25">วันที่___<u><?=$vc[vc_date2]?></u>___</td>
 	 </tr>
     <tr  align="center">
   	 <td>&nbsp;</td>
   	 <td height="25"></td>
 	 </tr>
	</table>
    </td>
  </tr>

  <tr>
   <td align="center">**********************************************************************************</td>
   <td>&nbsp;</td>
  </tr>
    <tr>
    <td>
<table width="100%" cellpadding="3" cellspacing="3"  align="center">
  <tr align="center" >
    <td width="50%"><b>คำสั่ง</b></td>
  </tr>
  <tr>
    <td align="center" ><input type="radio" name="app3" value="1" <? if(trim($vc["vc_status3"])== '1') echo "checked";?> disabled="disabled"/>อนุญาต &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="app3" value="2" <? if(trim($vc["vc_status3"])== '2') echo "checked";?> disabled="disabled" />ไม่อนุญาต</td>
  </tr>
  <tr align="center">
    <td height="25">
	<? if($vc[vc_boss]== ""){?>(ลงชื่อ)___________________________ผู้ออกคำสั่ง</td>
	<? } else if($vc[vc_boss] != ""){?>(ลงชื่อ)___<u><?=$boss["emp_title"]?><?=$boss["emp_name"]?>&nbsp;<?=$boss["emp_lname"]?></u>___</td> <? }?>
   </tr>
  <tr  align="center">
  <td height="25">
	<? if($vc[vc_boss]== ""){?>ตำแหน่ง_______________________</td>
	<? } else if($vc[vc_boss] != ""){?>ตำแหน่ง___<u><?=$boss[emp_position]?></u>___</td> <? }?>
  </tr>
  <tr  align="center">
    <td>วันที่__<u><?=$vc[vc_date4]?></u>__</td>
  </tr>
   <tr align="center">
    <td height="30">&nbsp;</td>
  </tr>
</table>
</table>

</center>
</center>
</table>
<? mysql_close(); ?>
</font>
</body>
</html>
