<?php
require_once('ThaiPDF/thaipdf.php');
ob_start();
?>
<html>
<head>

<style>
	caption{font-family: garuda;}
	table { border-collapse: collapse; margin: auto; font-family: garuda;}
	th { background-color:#cdf; }
	th, td { padding: 10px; width: 150px;  }
	td { vertical-align:top; line-height: 150%;}
	caption { text-align: center; }
	tr:nth-of-type(odd) { background-color: #ddd; }
	.td-right{ text-align: right;}
	.table-2{font-size: 14px;}
	u {
    border-bottom: 1px dotted #000;
    text-decoration: none;
	}
.br2{ line-height:1.8; }

</style>

</head>
<body>
<?php
require_once('function.php');
require_once('connect.php');
$id = $_GET["data"];
//echo $id ;
$wt=select("tblworktime","where id = '$id' ");
$emp=select("tblemp","where emp_id = '$wt[emp_id]' ");
$leader=select("tblemp","where emp_id = '$wt[wt_leader]' ");
$num_r=num_record("tblworktime","where id < '$id' and emp_id = '$wt[emp_id]' and wt_status1 = '1' and wt_cancel != '1' ");
 ?>
<table border="1" width="800">
<tr><td align = "center">
<caption style="font-weight: bold;">แบบขออนุญาตไปทำธุระส่วนตัวในเวลาปฏิบัติงาน</caption>
<br>
	<table class="table-2" border="0" cellpadding="10" cellspadding="10" width="780" >
    <tr>
        <td></td>
        <td class="td-right">วันที่ <u>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $wt["wt_create_date"] ; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
					<br><br>ครั้งที่ขออนุญาต<u>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $num_r+1 ; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
				</td>
    </tr>
    <tr>
        <td colspan="2" style="line-height:22px;">
					ผู้ขออนุญาต ชื่อ-สกุล <u>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $emp["emp_title"].$emp["emp_name"]."&nbsp;".$emp["emp_lname"] ; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
					ตำแหน่ง <u>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $emp["emp_position"] ; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> <br><br>
					งาน <u>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $emp["emp_unit"] ; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u> สถาบันนวัตกรรมกาารเรียนรู้ <br><br>
					เวลาที่ขออนุญาตออก<u>&nbsp;&nbsp; <?php echo $wt["wt_out_time"]." น." ; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
					เวลากลับ<u>&nbsp;&nbsp; <?php echo $wt["wt_return_time"]." น." ; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
				</td>
    </tr>
    <tr>
        <td align="center">ลงชื่อ <u>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $emp["emp_title"].$emp["emp_name"]." ".$emp["emp_lname"]; ?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> <br><br> ผู้ขออนุญาต </td>

				<?php if ($wt[wt_status1]==1) { ?>
					<td align="center">ลงชื่อ <u>&nbsp;&nbsp;&nbsp;&nbsp; นายเสฏฐวุฒิ อุรา &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> <br><br> นักทรัพยากรบุคคล </td>
				<?php }else if($wt[wt_status1]==0) { ?>
						<td align="center">ลงชื่อ <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> <br><br> นักทรัพยากรบุคคล </td>
				<?php }?>
    </tr>
    <tr>
			<?php if ($wt[wt_status2]==1) { ?>
				<td align="center">ลงชื่อ <u>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $leader["emp_title"].$leader["emp_name"]."".$leader["emp_lname"]; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> <br><br> ผู้บังคับบัญชาชั้นต้น </td>
			<?php }else if($wt[wt_status2]==0) { ?>
					<td align="center" style="line-height=1.8em;">ลงชื่อ <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> <br><br> ผู้บังคับบัญชาชั้นต้น </td>
			<?php }?>
			<td></td>
    </tr>

	</table>
 </td></tr>
</table>
</body>
</html>
<?php
$html = ob_get_clean();
pdf_html($html);
pdf_echo();
?>
