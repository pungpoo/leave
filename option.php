<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<script language="javascript">
$(document).ready(function () {
    showHideOption1();
    $("[name=type]").change(function () {
        showHideOption1();
    });
});
function showHideOption1() {
    var option_type = $("[name=type]").val();
    if (option_type == "1" || option_type == "2") {
        $("#option").fadeIn();
        $("[name=halfday]").attr("disabled", false);	
    } else {
        $("#option").fadeOut();
        $("[name=halfday]").attr("disabled", true);
    }
}


$(document).ready(function () {
    showHideOption2();
    $("[name=type]").change(function () {
        showHideOption2();
    });
});
function showHideOption2() {
    var option_type2 = $("[name=type]").val();
    if (option_type2 == "1" || option_type2 == "2") {
        $("#option2").fadeIn();
        $("[name=halfday2]").attr("disabled", false);	
    } else {
        $("#option2").fadeOut();
        $("[name=halfday2]").attr("disabled", true);
    }
}


$(document).ready(function () {
    showHideOption3();
    $("[name=type]").change(function () {
        showHideOption3();
    });
});
function showHideOption3() {
    var option_type = $("[name=type]").val();
    if (option_type == "2") {
        $("#reason").fadeIn();
        $("[name=reason]").attr("disabled", false);
    } else {
        $("#reason").fadeOut();
        $("[name=reason]").attr("disabled", true);	
    }
}


$(document).ready(function () {
    showHideOption4();
    $("[name=type]").change(function () {
        showHideOption4();
    });
});
function showHideOption4() {
    var option_type = $("[name=type]").val();
    if (option_type == "2") {
        $("#holiday").fadeIn();
        $("[name=holiday]").attr("disabled", false);
    } else {
        $("#holiday").fadeOut();
        $("[name=holiday]").attr("disabled", true);	
    }
}
</script>
</body>
</html>