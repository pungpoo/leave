<? session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LEAVE SYSTEM - VACATION</title>
<style type="text/css">
p {
	text-align: center;
}

/* Style the tab */
div.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
div.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
div.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}

</style>
</head>

<body background="image/wp2.jpg" style="background-size:cover">
<? $id = $_SESSION["id"] ;

require_once('function.php');
require_once('connect.php');
include 'thaidate.php';
include 'thaidatecon.php';
include 'timediff.php';
include 'head_menu.php';
$date = date('Y/m/d');
$datethai =  ThaiEachDate("$date");


$vc=select("tblvacation","where id = '".$_GET["id"]."' ");
//$vc_last=select("tblvacation","where emp_id = '$id' and vc_cancel != '1' and vc_period = '2559' order by id desc limit   ");
$emp=select("tblemp","where emp_id = '$id' ");
$leader=select("tblemp","where emp_id = '".$vc[vc_leader]."' ");

$vc_start = select("tblvacation"," where emp_id = '$id' AND vc_period = '2560' ORDER BY  `id` ASC LIMIT 1 ");
?>
<center>
<div class="tab" style="width:1024px;">
  <button class="tablinks" onclick="openCity(event, '2560')" id="defaultOpen">ปีงบประมาณ 2560</button>
  <button class="tablinks" onclick="openCity(event, '2561')">ปีงบประมาณ 2561</button>
</div>

<table width="1024" border="1" bordercolor="#000000" align="center" bgcolor="#FFFFFF"><tr><td>
<br /><br />

<center>
<font size="-1">

<table width="700" height="287" cellpadding="5" cellspacing="5" >

  <tr>
    <td align="center"><br /><font size="+2"><b>แบบใบลาพักร้อน</b></font></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right">เลขที่ใบลา : <?=$vc[vc_id]?></td>
  </tr>
  <tr>
    <td align="right">วัน/เดือน/ปี : <?=$datethai?></td>
  </tr>
  <tr>
    <td><b>เรื่อง ขอลาพักร้อน</b></td>
  </tr>
  <tr>
    <td><b>เรียน</b> ผู้อำนวยการสถาบันนวัตกรรมการเรียนรู้ </td>
  </tr>
  <tr>
    <td><font color="#FFFFFF">.</font> </td>
  </tr>
  <tr>
    <td>
    ข้าพเจ้า___<u><?=$emp["emp_sex"]?><?=$emp["emp_name"]?>&nbsp;<?=$emp["emp_lname"]?></u>____&nbsp;&nbsp;
    ตำแหน่ง___<u><?=$emp["emp_position"]?></u>________
   </td>
  </tr>
  <tr>
    <td> สังกัด___<u><?=$emp["emp_unit"]?></u>________</td>
  </tr>
  <tr>
    <td>มีสิทธิลาพักผ่อนในปีนี  <?=$vc_start[vc_balance]?> วันทำการ</td>
  </tr>
  <tr>
   	<td>ขอลาพักร้อนตั้งแต่วันที่__<u><?=$vc["vc_start"]?></u>__ถึง__<u><?=$vc["vc_end"]?></u>__  รวมเป็นเวลา __<u><?=$vc["vc_day"]?></u>__ วัน
        </td>
  </tr>
  <!--<tr>
    <td>ขอลาพักผ่อนตั้งแต่วันที่.....<?=Datethai($rowvc["vc_start"])?>..... ถึงวันที่ ..... <?=Datethai($rowvc["vc_end"])?>.....  &nbsp;มีกำหนด...<?=$rowvc["vc_day"]?>...วัน</td>
  </tr>-->
  <tr>
    <td>ในระหว่างลาจะติดต่อข้าพเจ้าได้ที่เบอร์โทรศัพท์ __<u><?=$emp["emp_tel"]?></u>__&nbsp;หรือ E-mail _<u><?=$emp["emp_mail"]?></u>_</td>
  </tr>


	<tr height="50"><td> <br /><br /><center>สถิติการลาพักร้อน <br />	<br />
    <table width="400" border="1" cellpadding="5" cellspacing="5" bordercolor="#000000" align="center">
  <tr align="center" height="20">
    <td>&nbsp;</td>
    <td><b>ลามาแล้ว</b></td>
    <td><b>ลาครั้งนี้</b></td>
     <td><b>รวมเป็น</b></td>

  </tr>
  <tr align="center" height="20">
    <td width=20%>จำนวน (วัน)</td>
    <td width=20%> <?=$vc["vc_totalday"]-$vc["vc_day"]?></td>
    <td width=20%><?=$vc["vc_day"]?></td>
    <td width=20%><?=$vc["vc_totalday"]?></td>
  </tr>
	</table>
    </center>
    </td></tr>
    <tr height="50"><td>&nbsp;</td></tr>
	<tr>
    <td>
	<table width="100%" cellpadding="0" cellspacing="0"  align="center">
  	<tr align="center" height="35">
    	<td >&nbsp;</td>
    	<td ><input type="radio" name="app1" value="1"<? if(trim($vc["vc_status1"])== '1') echo "checked";?> disabled="disabled"> ตรวจสอบแล้ว </td>
  	</tr>

  	<tr align="center" >
   	 <td width="50%" height="30">(ลงชื่อ)___<u><?=$emp["emp_sex"]?><?=$emp["emp_name"]?>&nbsp;<?=$emp["emp_lname"]?></u>___</td>
    <td width="50%" height="30">(ลงชื่อ)__________________ ผู้ตรวจสอบ</td>
  	</tr>
  	<tr align="center">
   	 <td >ตำแหน่ง__<u><?=$emp["emp_position"]?></u>__</td>
   	 <td height="25">ตำแหน่ง __________________</td>
   	</tr>
  	<tr  align="center">
   	 <td>&nbsp;</td>
   	 <td height="25">วันที่__________________</td>
 	 </tr>
	</table>
    </td>
  </tr>

  <tr>
   <td align="center">**********************************************************************************</td>
   <td>&nbsp;</td>
  </tr>
    <tr>
    <td>
<table width="100%" cellpadding="0" cellspacing="0"  align="center">
  <tr align="center" >
    <td width="50%" height="30"><b>ความเห็นผู้บังคับบัญชา</b></td>
    <td width="50%"><b>คำสั่ง</b></td>
  </tr>

  <tr>
    <td align="center" height="30"><input type="radio" name="app2" value="1"<? if(trim($vc["vc_status2"])== '1') echo "checked";?> disabled="disabled"/>อนุญาต &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="app2" value="2" <? if(trim($vc["vc_status2"])== '2') echo "checked";?> disabled="disabled"/>ไม่อนุญาต</td>
    <td align="center" ><input type="radio" name="app3" value="1" <? if(trim($vc["vc_status3"])== '1') echo "checked";?> disabled="disabled"/>อนุญาต &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="app3" value="2" <? if(trim($vc["vc_status3"])== '2') echo "checked";?> disabled="disabled" />ไม่อนุญาต</td>
  </tr>
  <tr align="center">
    <td height="30" align="center">(ลงชื่อ)__________________ ผู้บังคับบัญชา</td>
    <td >(ลงชื่อ)__________________ผู้ออกคำสั่ง</td>
   </tr>
  <tr  align="center">
    <td height="25">ตำแหน่ง __________________</td>
    <td>ตำแหน่ง __________________</td>
  </tr>
  <tr  align="center">
    <td  height="25" >วันที่__________________</td>
    <td>วันที่__________________</td>
  </tr>
   <tr align="center">
    <td height="30">&nbsp;</td>
    <td >&nbsp;</td>
  </tr>
	<? mysql_close(); ?>
</table>
</table>
<a href ="L_print_vc.php?data=<?=$vc["id"];?>" target="_blank"><button class="btn-success">PRINT</button></a>
<br><br><br>
<font color='red'> <IMG SRC="image/update.gif" WIDTH=50 HEIGHT=15>  *หมายเหตุ : เมื่อทำการพิมพ์ใบลาเรียบร้อยแล้ว นำไปให้ผู้บังคับบัญชาเซ็นชื่ออนุมัติเป็นลำดับต่อไป</font>
<br><br>
</center>
</font>
</body>
</html>
