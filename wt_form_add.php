<? session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LEAVE SYSTEM - Worktime-leave</title>
<style type="text/css">
p {
	text-align: center;
}
</style>

<!-- timepicker -->
<link rel="stylesheet" href="include/ui-1.10.0/ui-lightness/jquery-ui-1.10.0.custom.min.css" type="text/css" />
<link rel="stylesheet" href="jquery.ui.timepicker.css?v=0.3.3" type="text/css" />
<script type="text/javascript" src="include/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="include/ui-1.10.0/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="jquery.ui.timepicker.js?v=0.3.3"></script>

<style type="text/css">
		/* some styling for the page */
	body { font-size: 14px; /* for the widget natural size */ }
		#content { font-size: 1.2em; /* for the rest of the page to show at a normal size */
							 font-family: "Lucida Sans Unicode", "Lucida Grande", Verdana, Arial, Helvetica, sans-serif;
							/* width: 950px; margin: auto; */
		}

		fieldset { padding: 0.5em 2em }
		hr { margin: 0.5em 0; clear: both }
		a { cursor: pointer; }
		#requirements li { line-height: 1.6em; }

		.myButton {
			-moz-box-shadow:inset 0px 1px 0px 0px #97c4fe;
			-webkit-box-shadow:inset 0px 1px 0px 0px #97c4fe;
			box-shadow:inset 0px 1px 0px 0px #97c4fe;
			background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #3d94f6), color-stop(1, #1e62d0));
			background:-moz-linear-gradient(top, #3d94f6 5%, #1e62d0 100%);
			background:-webkit-linear-gradient(top, #3d94f6 5%, #1e62d0 100%);
			background:-o-linear-gradient(top, #3d94f6 5%, #1e62d0 100%);
			background:-ms-linear-gradient(top, #3d94f6 5%, #1e62d0 100%);
			background:linear-gradient(to bottom, #3d94f6 5%, #1e62d0 100%);
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#3d94f6', endColorstr='#1e62d0',GradientType=0);
			background-color:#3d94f6;
			-moz-border-radius:6px;
			-webkit-border-radius:6px;
			border-radius:6px;
			border:1px solid #337fed;
			display:inline-block;
			cursor:pointer;
			color:#ffffff;
			font-family:Arial;
			font-size:15px;
			font-weight:bold;
			padding:6px 24px;
			text-decoration:none;
			text-shadow:0px 1px 0px #1570cd;
		}
		.myButton:hover {
			background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #1e62d0), color-stop(1, #3d94f6));
			background:-moz-linear-gradient(top, #1e62d0 5%, #3d94f6 100%);
			background:-webkit-linear-gradient(top, #1e62d0 5%, #3d94f6 100%);
			background:-o-linear-gradient(top, #1e62d0 5%, #3d94f6 100%);
			background:-ms-linear-gradient(top, #1e62d0 5%, #3d94f6 100%);
			background:linear-gradient(to bottom, #1e62d0 5%, #3d94f6 100%);
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#1e62d0', endColorstr='#3d94f6',GradientType=0);
			background-color:#1e62d0;
		}
		.myButton:active {
			position:relative;
			top:1px;
		}
		.left{
			text-align: left;
			margin-top: 10px;
		}
		.right{
			text-align: right;
			margin-top: 10px;
		}
		.center{
			text-align: center;
		}
	.container {
  width:100%;
	height: auto;
	padding: 10px;
  border:2px solid #7F8C8D; /* กำหนดกรอบให้เห็นภาพ */
	border-radius: 15px;
  position:relative; /* กำหนดให้เป็น container */
}
.textbox{
	width:100px;
	height: auto;
	padding: 10px;
	font-size: 16px;
	border-radius: 15px;
	border:2px solid #7F8C8D;
	font-weight: bold;
}
.label{
	font-size: 20px;
	font-weight: bold;
}
</style>

<!-- end timepicker -->

</head>
<body background="image/wp2.jpg" style="background-size:cover">
<? $id = $_SESSION["id"] ;

require_once('function.php');
require_once('connect.php');
include 'thaidate.php';
include 'thaidatecon.php';

include 'head_menu.php';

$date = date('Y/m/d');
$datethai =  ThaiEachDate("$date");

$emp = select("tblemp","where emp_id = '$id' ");
$wt = select("tblworktime","where emp_id = '$id' ORDER BY id DESC limit 1");
$sum_wt = num_record("tblworktime","where emp_id = '$id'  and wt_status2 = 1 and wt_cancel = 0");

//echo $wt_id_new;
$y = substr((date('Y'))+543, 2, 2); //แก้ปีงบฯตรงนี้
$wtid=select("tblworktime","ORDER BY id DESC limit 1");
if($wtid[wt_id]){
	$e = substr($wtid[wt_id],5);
	$e = $e+1;
	//echo $e ."<br>";
	}
else if(!$wt_id[wt_id]){
	$e = 1;
	//echo $e;
	}
$wt_id = "wt".$y."-".$e;
//echo $wt_id ;
/* บันทึกใบลา */

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$t1 = substr($_POST["t1"],0,2);
	$t2 = substr($_POST["t2"],0,2);

		if($_POST["t1"] != "" and $_POST["t2"] != "" and $t2 >= $t1)
		{
				$str = "INSERT INTO tblworktime (wt_id,emp_id,wt_create_date,wt_out_time,wt_return_time,wt_status1,wt_status2,wt_leader,wt_period)
				VALUES('$wt_id','".$emp["emp_id"]."','$datethai','".$_POST["t1"]."','".$_POST["t2"]."','0','0','".$emp["emp_leader"]."','2560') ";
				$check = mysql_query($str);
						if($check)
							{
							echo"<script language='JavaScript'>";
							echo"alert('บันทึกคำขอเรียบร้อย');";
							echo"window.location.href='user_home_wt.php';";
							echo"</script>";
							include("mail_worktime.php");
							}
							else{echo "error"; echo $str ; echo $row["emp_id"]; }
		}
		else
		{
				echo"<script language='JavaScript'>";
				echo"alert('ขออภัย!! คุณเลือกเวลาไม่ถูกต้อง');";
				echo"window.location='wt_form_add.php';";
				echo"</script>";
		}
}
?>
<center>
<table width="1024" border="1" bordercolor="#000000" align="center" bgcolor="#FFFFFF" style="background-color: #ffffff;"><tr><td>
<center>
<table width="700" height="287"  border="0" bordercolor="#000000" >
  <tr>
    <td>
			<br /><font size="+2"><p><b>คำขออนุญาติไปทำธุระส่วนตัวในเวลาปฏิบัติงาน</b></p></font>
			<br><br>
			<div class="container">
			<p class="left">ผู้ใช้ระบบ : <?php echo $emp[emp_name]." ".$emp[emp_lname]; ?> </p>
			<p class="right">วัน/เดือน/ปี : <?php echo $datethai; ?> </p>
			<p class="right">ครั้งที่ขออนุญาต :<b> <?php echo $sum_wt+1; ?> </b></p>
				<br>
		<div class="center">
		<form name="f1" method="post" action="#">
		<label for="timepicker.[1]" class="label">ระบุเวลา :</label>
		<input type="text" class="textbox" name="t1" id="timepicker.[1]" value="" placeholder="เวลาออก" />
		&nbsp;	&nbsp;
		<input type="text" class="textbox" name="t2" id="timepicker.[2]" value="" placeholder="เวลากลับ" />
		&nbsp; <br><br>
		<input type="submit" name="submit" value="บันทึก" class="myButton" onClick="return confirm('ยืนยันการบันทึกคำขอ !');" />
		<script type="text/javascript">
				$(document).ready(function() {
						$('#timepicker\\.\\[1\\]').timepicker( {
							//showAnim: 'blind'
						} );
				});
				$(document).ready(function() {
						$('#timepicker\\.\\[2\\]').timepicker( {
						} );
				});
		</script>
	</form>

	<?php
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		echo $_POST['t1'] . "<br>";
		echo $_POST['t2'] . "<br>";
		}
	?>
</div>
</div>
		</td>
  </tr>
</table>
<br><br>
</center>
</center>
</body>
</html>
