<?php

// Copyright (C) 2005 Ilya S. Lyubinskiy. All rights reserved.
// Technical support: http://www.php-development.ru/
//
// YOU MAY NOT
// (1) Remove or modify this copyright notice.
// (2) Distribute this code, any part or any modified version of it.
//     Instead, you may link to the homepage of this code:
//     http://www.php-development.ru/javascripts/form-validation.php.
//
// YOU MAY
// (1) Use this code on your website.
// (2) Use this code as a part of another product provided that
//     its main use is not html form processing.
//
// NO WARRANTY
// This code is provided "as is" without warranty of any kind, either
// expressed or implied, including, but not limited to, the implied warranties
// of merchantability and fitness for a particular purpose. You expressly
// acknowledge and agree that use of this code is at your own risk.


// If you find my script useful, you can support my site in the following ways:
// 1. Vote for the script at HotScripts.com (you can do it on my site)
// 2. Link to the homepage of this script or to the homepage of my site:
//    http://www.php-development.ru/javascripts/form-validation.php
//    http://www.php-development.ru/
//    You will get 50% commission on all orders made by your referrals.
//    More information can be found here:
//    http://www.php-development.ru/affiliates.php


// ***** Alerts ****************************************************************

$form_validation_alerts = Array(
    '>'      => "%%Name%% should be more than %%num%%!",
    '<'      => "%%Name%% should be less than %%num%%!",
    '>='     => "%%Name%% should be more or equal to %%num%%!",
    '<='     => "%%Name%% should be less or equal to %%num%%!",
    'ch'     => "%%Name%% contains invalid characters!",
    'chnum_' => "%%Name%% contains invalid characters!",
    'cnt >'  => "You should select more than %%num%% %%name%%!",
    'cnt <'  => "You should select less than %%num%% %%name%%!",
    'cnt >=' => "You should select at least %%num%% %%name%%!",
    'cnt <=' => "You should select at most %%num%% %%name%%!",
    'cnt ==' => "You should select %%num%% %%name%%!",
    'date'   => "Please, enter a valid %%name%%!",
    'email'  => "Please, enter a valid e-mail address!",
    'empty'  => "Please, enter %%name%%!",
    'len >'  => "%%Name%% should contain more than %%num%% characters!",
    'len <'  => "%%Name%% should contain less than %%num%% characters!",
    'len >=' => "%%Name%% should contain at least %%num%% characters!",
    'len <=' => "%%Name%% should contain at most %%num%% characters!",
    'len ==' => "%%Name%% should contain %%num%% characters!",
    'num'    => "%%Name%% is not a valid number!",
    'radio'  => "Please, select %%name%%!",
    'select' => "Please, select %%name%%!",
    'terms'  => "You must agree to the terms first!");

function form_validation_alert($type, $name, $num)
{
  $name = preg_replace('/^\W*(\w*)\W*$/', "$1", $name);

  $msg = $GLOBALS['form_validation_alerts'][$type];
  $msg = str_replace('%%Name%%', strtoupper(substr($name, 0, 1)) . strtolower(substr($name, 1, strlen($name)-1)), $msg);
  $msg = str_replace('%%name%%', strtolower($name), $msg);
  $msg = str_replace('%%num%%', $num, $msg);

  return $msg;
}

// ***** isNaN *****************************************************************

function form_validation_isNaN($value)
{
  return (string)(integer)$value !== (string)$value;
}

// ***** Validate **************************************************************

function form_validation_validate($data, $rules)
{
  $rules = preg_replace('/^(\s*)(\S.*)/', "$2", $rules);
  $rules = preg_split('/\s*;\s*/', $rules);

  foreach ($rules as $i => $rule)
  {
    $rule = preg_split('/\s*:\s*/', $rule);

    if (count($rule) < 2) continue;

    $rule[0] = preg_split('/\s+/', $rule[0]);
    $rule[1] = preg_split('/\s+/', $rule[1]);

    foreach ($rule[0] as $j => $name)
    {
      $name = str_replace("[]", "", $name);

      if (!in_array($rule[1][0], Array('cnt', 'radio', 'terms')))
        if (!isset($data[$name])) return 'Invalid form!';

      if (!in_array($rule[1][0], Array('cnt')))
        if ( isset($data[$name]) && is_array($data[$name])) return 'Invalid form!';

      switch ($rule[1][0])
      {
        // ***** Comparison *****

        case '>':
          if (form_validation_isNaN($data[$name]))
            return form_validation_alert('num', $name, 0);
          if ($data[$name] <= $rule[1][1])
            return form_validation_alert('>', $name, $rule[1][1]);
          break;

        case '<':
          if (form_validation_isNaN($data[$name]))
            return form_validation_alert('num', $name, 0);
          if ($data[$name] >= $rule[1][1])
            return form_validation_alert('<', $name, $rule[1][1]);
          break;

        case '>=':
          if (form_validation_isNaN($data[$name]))
            return form_validation_alert('num', $name, 0);
          if ($data[$name] < $rule[1][1])
            return form_validation_alert('>=', $name, $rule[1][1]);
          break;

        case '<=':
          if (form_validation_isNaN($data[$name]))
            return form_validation_alert('num', $name, 0);
          if ($data[$name] > $rule[1][1])
            return form_validation_alert('<=', $name, $rule[1][1]);
          break;

        // ***** Ch *****

        case 'ch':
          if (!preg_match('/^([A-Za-z]+)$/', $data[$name]))
            return form_validation_alert('ch', $name, 0);
          break;

        // ***** Chnum_ *****

        case 'chnum_':
          if (!preg_match('/^(\w+)$/', $data[$name]))
            return form_validation_alert('chnum_', $name, 0);
          break;

        // ***** Cnt *****

        case 'cnt':
          $cnt = isset($data[$name]) ? (is_array($data[$name]) ? count($data[$name]) : 1) : 0;
          if ($rule[1][1] == '>' && $cnt <= $rule[1][2])
            return form_validation_alert('cnt >', $name, $rule[1][2]);
          if ($rule[1][1] == '<' && $cnt >= $rule[1][2])
            return form_validation_alert('cnt <', $name, $rule[1][2]);
          if ($rule[1][1] == '>=' && $cnt < $rule[1][2])
            return form_validation_alert('cnt >=', $name, $rule[1][2]);
          if ($rule[1][1] == '<=' && $cnt > $rule[1][2])
            return form_validation_alert('cnt <=', $name, $rule[1][2]);
          break;

        // ***** Email *****

        case 'email':
          if (!preg_match('/^(\w+\.)*(\w+)@(\w+\.)+(\w+)$/', $data[$name]))
            return form_validation_alert('email', $name, 0);
          break;

        // ***** Empty *****

        case 'empty':
          if ($data[$name] == '')
            return form_validation_alert('empty', $name, 0);
          break;

        // ***** Len *****

        case 'len':
          if ($rule[1][1] == '>' && strlen($data[$name]) <= $rule[1][2])
            return form_validation_alert('len >', $name, $rule[1][2]);
          if ($rule[1][1] == '<' && strlen($data[$name]) >= $rule[1][2])
            return form_validation_alert('len <', $name, $rule[1][2]);
          if ($rule[1][1] == '>=' && strlen($data[$name]) < $rule[1][2])
            return form_validation_alert('len >=', $name, $rule[1][2]);
          if ($rule[1][1] == '<=' && strlen($data[$name]) > $rule[1][2])
            return form_validation_alert('len <=', $name, $rule[1][2]);
          break;

        // ***** Num *****

        case 'num':
          if (form_validation_isNaN($data[$name]))
            return form_validation_alert('num', $name, 0);
          break;

        // ***** Radio *****

        case 'radio':
          if (!isset($data[$name]))
            return form_validation_alert('radio', $name, 0);
          break;

        // ***** Select *****

        case 'select':
          if (!isset($data[$name]))
            return form_validation_alert('select', $name, 0);
          break;

        // ***** Terms *****

        case 'terms':
          if (!isset($data[$name]))
            return form_validation_alert('terms', $name, 0);
          break;
      }
    }

    // ***** Date *****

    if ($rule[1][0] == 'date')
    {
      $year  = $rule[0][0];
      $month = $rule[0][1];
      $day   = $rule[0][2];

      if (form_validation_isNaN($data[$year ]))
        return form_validation_alert('date', $year,  0);
      if (form_validation_isNaN($data[$month]) || $data[$month] < 0 || $data[$month] > 12)
        return form_validation_alert('date', $month, 0);
      if (form_validation_isNaN($data[$day  ]) || $data[$day  ] < 0 || $data[$day  ] > 31 || !checkdate($data[$month], $data[$day], $data[$year]))
        return form_validation_alert('date', $day,   0);
    }
  }

  return true;
}

?>
