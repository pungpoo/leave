<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml2/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="Author" content="Stu Nicholls" />

<link href="menu/css/reset.css" rel="stylesheet" type="text/css"/>
<!--<link href="menu/css/960.css" rel="stylesheet" type="text/css"/> -->
<link href="menu/css/coolMenu.css" rel="stylesheet" type="text/css" media="screen"/>
<script type="text/javascript" src="menu/js/modernizr-1.6.min.js"></script>
</head>
<body>

<center>

<table width="1024" border="1" cellpadding="0" cellspacing="0" align="center" style="background:#2f8be8">
<tr >
<td align="center"><img src="image/logo.png"  width="1024" /></td>
</tr>
<tr>
<td bgcolor="#2f8be8">
	<div class="container_16">
		<ul id="coolMenu">
			<!-- <li><a href="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;หน้าหลัก&nbsp;&nbsp;&nbsp;</a></li> -->
			<li><a href="user_profile.php">&nbsp;&nbsp;&nbsp;ข้อมูลส่วนตัว&nbsp;&nbsp;&nbsp;</a></li>
			<li><a href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; สถิติการลา &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
				<ul class="noJS">
				<li type="disc" ><a href="user_home.php" style="width:auto">ลาป่วย ลากิจ</a></li>
				<li type="disc" ><a href="user_home_vc.php" style="width:auto">ลาพักร้อน</a></li>
				</ul>
			</li>  
            <li><a href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; สร้างใบลา &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
				<ul class="noJS">
				<li type="disc"><a href="L_form_add.php" style="width:auto">ลาป่วย ลากิจ</a></li>
				<li type="disc"><a href="vc_form_add.php" style="width:auto">ลาพักร้อน</a></li>
				</ul>
			</li>
			<li><a href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; อนุมัติผู้ใต้บังคับบัญชา &nbsp;&nbsp;&nbsp;</a>
				<ul class="noJS">
				<li type="disc"><a href="boss_show.php" style="width:auto">อนุมัติการลาป่วย ลากิจ</a></li>
				<li type="disc"><a href="boss_show_vc.php" style="width:auto">อนุมัติการลาพักร้อน</a></li>
				<li type="disc"><a href="boss_show_urt.php" style="width:auto">อนุมัติการขอบันทึกเวลาปฏิบัติงาน</a></li>
				</ul>
			</li>
			<li><a href="#"> ข้อมูลการลา </a>
				<ul class="noJS">
				<li type="disc" ><a href="stat_leave.php" style="width:auto">สถิติการลาป่วย ลากิจ ของบุคลากร</a></li>
				<li type="disc" ><a href="stat_vc.php?ntime=<?=time();?>" style="width:auto">สถิติการลาพักร้อน ของบุคลากร</a></li>
				<li type="disc"><a href="stat_urt.php" style="width:auto">สถิติการขอบันทึกเวลาปฏิบัติงาน ของบุคลากร</a></li>
				<li type="disc" ><a href="report_61.php" style="width:auto" target="_blank">สรุปการลาของบุคลากร</a></li>
                </ul>
			</li>			
			<li><a href="index.php" >&nbsp;&nbsp;ออกจากระบบ&nbsp;&nbsp;</a></li>
		</ul>
	</div>
</td>
</tr>
</table>
</center>
</body>
</html>
