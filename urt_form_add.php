<? session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LEAVE SYSTEM - Worktime-leave</title>
<style type="text/css">
p {
	text-align: center;
}
</style>

<!-- timepicker -->
<link rel="stylesheet" href="include/ui-1.10.0/ui-lightness/jquery-ui-1.10.0.custom.min.css" type="text/css" />
<link rel="stylesheet" href="jquery.ui.timepicker.css?v=0.3.3" type="text/css" />
<script type="text/javascript" src="include/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="include/ui-1.10.0/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="jquery.ui.timepicker.js?v=0.3.3"></script>

<link rel="stylesheet" href="css/jquery.datetimepicker.css">

<style type="text/css">
		/* some styling for the page */
	body { font-size: 14px; /* for the widget natural size */ }
		#content { font-size: 1.2em; /* for the rest of the page to show at a normal size */
							 font-family: "Lucida Sans Unicode", "Lucida Grande", Verdana, Arial, Helvetica, sans-serif;
							/* width: 950px; margin: auto; */
		}

		fieldset { padding: 0.5em 2em }
		hr { margin: 0.5em 0; clear: both }
		a { cursor: pointer; }
		#requirements li { line-height: 1.6em; }

		.myButton {
			-moz-box-shadow:inset 0px 1px 0px 0px #97c4fe;
			-webkit-box-shadow:inset 0px 1px 0px 0px #97c4fe;
			box-shadow:inset 0px 1px 0px 0px #97c4fe;
			background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #3d94f6), color-stop(1, #1e62d0));
			background:-moz-linear-gradient(top, #3d94f6 5%, #1e62d0 100%);
			background:-webkit-linear-gradient(top, #3d94f6 5%, #1e62d0 100%);
			background:-o-linear-gradient(top, #3d94f6 5%, #1e62d0 100%);
			background:-ms-linear-gradient(top, #3d94f6 5%, #1e62d0 100%);
			background:linear-gradient(to bottom, #3d94f6 5%, #1e62d0 100%);
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#3d94f6', endColorstr='#1e62d0',GradientType=0);
			background-color:#3d94f6;
			-moz-border-radius:6px;
			-webkit-border-radius:6px;
			border-radius:6px;
			border:1px solid #337fed;
			display:inline-block;
			cursor:pointer;
			color:#ffffff;
			font-family:Arial;
			font-size:15px;
			font-weight:bold;
			padding:6px 24px;
			text-decoration:none;
			text-shadow:0px 1px 0px #1570cd;
		}
		.myButton:hover {
			background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #1e62d0), color-stop(1, #3d94f6));
			background:-moz-linear-gradient(top, #1e62d0 5%, #3d94f6 100%);
			background:-webkit-linear-gradient(top, #1e62d0 5%, #3d94f6 100%);
			background:-o-linear-gradient(top, #1e62d0 5%, #3d94f6 100%);
			background:-ms-linear-gradient(top, #1e62d0 5%, #3d94f6 100%);
			background:linear-gradient(to bottom, #1e62d0 5%, #3d94f6 100%);
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#1e62d0', endColorstr='#3d94f6',GradientType=0);
			background-color:#1e62d0;
		}
		.myButton:active {
			position:relative;
			top:1px;
		}
		.left{
			text-align: left;
			margin-top: 10px;
		}
		.right{
			text-align: right;
			margin-top: 10px;
		}
		.center{
			text-align: center;
		}
	.container {
  width:100%;
	height: auto;
	padding: 10px;
  border:2px solid #7F8C8D; /* กำหนดกรอบให้เห็นภาพ */
	border-radius: 15px;
  position:relative; /* กำหนดให้เป็น container */
}
.textbox{
	width:200px;
	height: auto;
	padding: 10px;
	font-size: 16px;
	border-radius: 15px;
	border:2px solid #7F8C8D;
	font-weight: bold;
}
.label{
	font-size: 20px;
	font-weight: bold;
}
.textarea{
	padding: 10px;
	font-size: 16px;
	border-radius: 15px;
	border:2px solid #7F8C8D;
	font-weight: bold;
	
}

</style>

<!-- end timepicker -->

</head>
<body background="image/wp2.jpg" style="background-size:cover">
<? $id = $_SESSION["id"] ;

require_once('function.php');
require_once('connect.php');
include 'head_menu.php';
include 'hide_show.php';

$emp = select("tblemp","where emp_id = '$id' ");
$wt = select("tblworktime","where emp_id = '$id' ORDER BY id DESC limit 1");
$sum_wt = num_record("tblworktime","where emp_id = '$id'  and wt_status2 = 1 and wt_cancel = 0");

//echo $wt_id_new;
$y = substr((date('Y'))+543, 2, 2); //แก้ปีงบฯตรงนี้
$urtid=select("tblurt","ORDER BY id DESC limit 1");
if($urtid[urt_id]){
	$e = substr($urtid[urt_id],6);
	$e = $e+1;
	}
else if(!$urt_id[urt_id]){
	$e = 1;
	}
$urt_id = "urt".$y."-".$e;
/* บันทึกใบลา */
$create_date =  date("d/m/Y H:i:s");
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	
	/*คำนวณวัน*/
	function ThaiEachDate($vardate="") {   
	 $_month_name = array("01"=>"มกราคม",  "02"=>"กุมภาพันธ์",  "03"=>"มีนาคม",    
	    "04"=>"เมษายน",  "05"=>"พฤษภาคม",  "06"=>"มิถุนายน",    
	    "07"=>"กรกฎาคม",  "08"=>"สิงหาคม",  "09"=>"กันยายน",    
	    "10"=>"ตุลาคม", "11"=>"พฤศจิกายน",  "12"=>"ธันวาคม");  
	$yy =substr($vardate,6,4);$mm =substr($vardate,3,2);$dd =substr($vardate,0,2);    
	//$yy += 543;  
	 if ($yy==543){  
	  $dateT = "-";  
	 }else{  
	  $dateT=$dd ." ".$_month_name[$mm]."  ".$yy;  
	 }  
	  return $dateT;  
	}
	$datethai =  ThaiEachDate($_POST["datepick"]);
	$datethai2 =  ThaiEachDate($_POST["datepick2"]);
	echo $datethai2 ."<br />";
	/* Check Validation */
	if($_POST["datepick"] === ""){
                echo "<script language='javascript'>
                  alert('โปรดระบุวันที่')
                  history.back()
                  </script>";
    }else{
		/* Check Type */
		if($_POST["reason"]==='1'){
			if($_POST["timepicker1"] === "" or $_POST["timepicker2"] === ""){
	                echo "<script language='javascript'>
	                  alert('โปรดระบุเวลาให้ครบถ้วน')
	                  history.back()
	                  </script>";
	   		}
			$urt_time = $_POST["timepicker1"]." ถึง ".$_POST["timepicker2"] ;
		}elseif ($_POST["reason"]==='2') {
			if($_POST["timepicker3"] === ""){
	                echo "<script language='javascript'>
	                  alert('โปรดระบุเวลา')
	                  history.back()
	                  </script>";
	   		}
			$urt_time = $_POST["timepicker3"] ;
		}elseif ($_POST["reason"]==='3') {
			if($_POST["timepicker3"] === ""){
	                echo "<script language='javascript'>
	                  alert('โปรดระบุเวลา')
	                  history.back()
	                  </script>";
	   		}
			$urt_time = $_POST["timepicker3"] ;
		}elseif ($_POST["reason"]==='4') {
			if($_POST["timepicker4"] === "" or $_POST["timepicker5"] === "" or $_POST["detail"] === ""){
	                echo "<script language='javascript'>
	                  alert('โปรดระบุข้อมูลให้ครบถ้วน')
	                  history.back()
	                  </script>";
	   		}
			$urt_time = $_POST["timepicker4"]." ถึง  ".$_POST["timepicker5"] ;
			$urt_detail = $_POST["detail"];
		}/*---*/
		elseif
		($_POST["reason"]==='5') {
			if($_POST["datepick"] === "" or $_POST["datepick2"] === "" or $_POST["detail"] === ""){
	                echo "<script language='javascript'>
	                  alert('โปรดระบุข้อมูลให้ครบถ้วน')
	                  history.back()
	                  </script>";
	   		}
			$urt_time = "-";
			$urt_detail = $_POST["detail"];
			$datethai = $datethai."ถึง ".$datethai2 ;
	   	}
			
	}
			/* บันทึก 	*/		
				if($urt_time){
						$str = "INSERT INTO tblurt (urt_id,emp_id,leader_id,urt_date,urt_reason,urt_time,urt_detail,urt_period,urt_create_date)
						VALUES('$urt_id','".$emp["emp_id"]."','".$emp["emp_leader"]."','$datethai','".$_POST["reason"]."','$urt_time','$urt_detail','2561','$create_date') ";
						$check = mysql_query($str);
								if($check)
									{
									//echo $str;
									//echo $_POST["datepick2"];
									echo"<script language='JavaScript'>";
									echo"alert('บันทึกคำขอเรียบร้อย');";
									echo"window.location.href='user_home_urt.php';";
									echo"</script>";
									//include("mail_worktime.php");
									}
									else{echo "error"; echo $str ; echo $row["emp_id"]; }
				}
				else
				{
						echo"<script language='JavaScript'>";
						echo"alert('ขออภัย!! คุณเลือกเวลาไม่ถูกต้อง');";
						echo"window.location='urt_form_add.php';";
						echo"</script>";
				}
			
}
?>
<center>
<table width="1024" border="1" bordercolor="#000000" align="center" bgcolor="#FFFFFF" style="background-color: #ffffff;"><tr><td>
<center>
<table width="800" height="287"  border="0" bordercolor="#000000" >
  <tr>
    <td>
			<br /><font size="+2"><p><b>แบบรายงานเหตุผลกรณีไม่ได้บันทึกเวลาปฏิบัติงาน</b></p></font>
			<br><br>
			<div class="container">
			<br>
		<div>
		<form name="f1" method="post" action="#">
		<label class="label">เหตุผล / ความจำเป็น : </label>
		<select name="reason" id="reason" class="label" style="border-radius: 15px; text-align-last: center;">
			<option value="0">--เลือก--</option>
			<option value="1">ไม่ได้นำบัตรมา</option>
			<option value="2">ไม่ได้บันทึกเวลาเข้างาน</option>
			<option value="3">ไม่ได้บันทึกเวลาออกงาน</option>
			<option value="4">ออกไปปฏิบัติงานนอกสถานที่</option>
			<option value="5">ออกไปปฏิบัติงานนอกสถานที่เต็มวัน</option>
		</select><br><br>
		<div  style="display: inline-block;">
		<label class="label">วันที่ :</label>
		<input type="text" name="datepick" id="datepick" class="label" style="border-radius: 20px;  text-align-last: center;" placeholder="เลือกวันที่">
		</div>
		<div  id="option4"  style="display: inline-block;">
		<label class="label">ถึง</label>
		<input type="text" name="datepick2" id="datepick2" class="label" style="border-radius: 20px;  text-align-last: center;" placeholder="เลือกวันที่">
		</div>
		
		
		<div id="option1" ><br />
			<label class="label">ระบุเวลา :</label>
			<input type="text" class="textbox" name="timepicker1" id="timepicker1" value="" placeholder="เวลาเข้างานจริง" />
			&nbsp;	&nbsp;
			<input type="text" class="textbox" name="timepicker2" id="timepicker2" value="" placeholder="เวลาออกงานจริง" /> 
		</div>
		
		<div id="option2" ><br />
			<label for="timepicker.[1]" class="label">ระบุเวลา :</label>
			<input type="text" class="textbox" name="timepicker3" id="timepicker3" value="" placeholder="ระบุเวลาที่ต้องการบันทึก" />
		</div>
		
		<div id="option3" ><br />
			<label for="timepicker.[1]" class="label">ระบุเวลา :</label>
			<input type="text" class="textbox" name="timepicker4" id="timepicker4" value="" placeholder="เวลาที่ออกไปปฏิบัติงาน" />
			&nbsp;	&nbsp;
			<input type="text" class="textbox" name="timepicker5" id="timepicker5" value="" placeholder="เวลาเลิกปฏิบัติงาน" /> 
		</div>
		<br /><br />
		
		<div  id="option5" >
		<label class="label">รายละเอียด : </label><br /><br />
			<textarea name="detail" id="detail" rows="4" cols="80" class="textarea" placeholder="ระบุรายละเอียด เช่น ชื่อโครงการ/ชื่องาน สถานที่ที่ไปปฏิบัติงาน "></textarea>
		</div>
		&nbsp; <br><br>
		<input type="submit" name="submit" value="บันทึก" class="myButton" onClick="return confirm('ยืนยันการบันทึกคำขอ !');" />

		<script type="text/javascript">
			$('#timepicker1').timepicker();
			$('#timepicker2').timepicker();
			$('#timepicker3').timepicker();
			$('#timepicker4').timepicker();
			$('#timepicker5').timepicker();
		</script>
	</form>
	</div>
</div>
		</td>
  </tr>
</table>
<br><br>
</center>
</center>

<!-- JS -->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/jquery.datetimepicker.full.js"></script>

<!-- ให้ Textbox คลิกแล้วเป็นปฏิทินให้เลือกวัน -->
<script type="text/javascript">
$(function(){
	$.datetimepicker.setLocale('th'); // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
	// กรณีใช้แบบ input
	$("#datepick").datetimepicker({
	timepicker:false,
	format:'d-m-Y', // กำหนดรูปแบบวันที่ ที่ใช้ เป็น 00-00-0000
	lang:'th', // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
	onSelectDate:function(dp,$input){
	var yearT=new Date(dp).getFullYear()-0;
	var yearTH=yearT+543;
	var fulldate=$input.val();
	var fulldateTH=fulldate.replace(yearT,yearTH);
	$input.val(fulldateTH);
	},
	});
	// กรณีใช้กับ input ต้องกำหนดส่วนนี้ด้วยเสมอ เพื่อปรับปีให้เป็น ค.ศ. ก่อนแสดงปฏิทิน
	$("#datepick").on("mouseenter mouseleave",function(e){
	var dateValue=$(this).val();
	if(dateValue!=""){
	var arr_date=dateValue.split("-"); // ถ้าใช้ตัวแบ่งรูปแบบอื่น ให้เปลี่ยนเป็นตามรูปแบบนั้น
	// ในที่นี้อยู่ในรูปแบบ 00-00-0000 เป็น d-m-Y แบ่งด่วย - ดังนั้น ตัวแปรที่เป็นปี จะอยู่ใน array
	// ตัวที่สอง arr_date[2] โดยเริ่มนับจาก 0
	if(e.type=="mouseenter"){
	var yearT=arr_date[2]-543;
	}
	if(e.type=="mouseleave"){
	var yearT=parseInt(arr_date[2])+543;
	}
	dateValue=dateValue.replace(arr_date[2],yearT);
	$(this).val(dateValue);
	}
	});
});
$(function(){
	$.datetimepicker.setLocale('th'); // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
	// กรณีใช้แบบ input
	$("#datepick2").datetimepicker({
	timepicker:false,
	format:'d-m-Y', // กำหนดรูปแบบวันที่ ที่ใช้ เป็น 00-00-0000
	lang:'th', // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
	onSelectDate:function(dp,$input){
	var yearT=new Date(dp).getFullYear()-0;
	var yearTH=yearT+543;
	var fulldate=$input.val();
	var fulldateTH=fulldate.replace(yearT,yearTH);
	$input.val(fulldateTH);
	},
	});
	// กรณีใช้กับ input ต้องกำหนดส่วนนี้ด้วยเสมอ เพื่อปรับปีให้เป็น ค.ศ. ก่อนแสดงปฏิทิน
	$("#datepick2").on("mouseenter mouseleave",function(e){
	var dateValue=$(this).val();
	if(dateValue!=""){
	var arr_date=dateValue.split("-"); // ถ้าใช้ตัวแบ่งรูปแบบอื่น ให้เปลี่ยนเป็นตามรูปแบบนั้น
	// ในที่นี้อยู่ในรูปแบบ 00-00-0000 เป็น d-m-Y แบ่งด่วย - ดังนั้น ตัวแปรที่เป็นปี จะอยู่ใน array
	// ตัวที่สอง arr_date[2] โดยเริ่มนับจาก 0
	if(e.type=="mouseenter"){
	var yearT=arr_date[2]-543;
	}
	if(e.type=="mouseleave"){
	var yearT=parseInt(arr_date[2])+543;
	}
	dateValue=dateValue.replace(arr_date[2],yearT);
	$(this).val(dateValue);
	}
	});
});
</script>
<!-- End JS -->
</body>
</html>
