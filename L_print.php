<? session_start(); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Leave System</title>
  <style type="text/css">
  div {
    font-size: 12px;
    /*font-family: serif;
    font-size:small; */
  }
  /*@page{size:auto; margin-bottom:5mm; margin-top:5mm;} */
  </style>

  <style type="text/css" media="print">
    @page
    {
        size:  auto;   /* auto is the initial value */
        margin: 5mm;  /* this affects the margin in the printer settings */
    }


    </style>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="AdminLTE2/bootstrap/css/bootstrap.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="AdminLTE2/dist/css/AdminLTE.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<!--  <body style="max-width:1000px;">  -->
<body style="max-width:1000px;">
  <? $id = $_SESSION["id"] ;
  require_once('function.php');
  require_once('connect.php');
  include 'thaidate.php';
  include 'thaidatecon.php';


  $date = date('Y/m/d');
  $datethai =  ThaiEachDate("$date");

  $leave=select("tblleave"," where id = '".$_GET["data"]."' ");
  $emp=select("tblemp","where emp_id =   '".$leave["emp_id"]."' ");
  $leader=select("tblemp","where emp_id = '".$leave["lf_leader"]."' ");
  $boss=select("tblemp","where emp_id = '".$leave["lf_boss"]."' ");
  ?>
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div>
        <h2 class="page-header" align = "center" style="font-family: serif;"> แบบใบลาป่วย ลาคลอดบุตร ลากิจส่วนตัว
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info" >
      <div class="col-sm-12" align = "right">
          (เขียนที่)<span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; สถาบันนวัตกรรมการเรียนรู้ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br>
          เลขที่ใบลา :   <span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$leave["lf_id"]?>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br>
          วัน/เดือน/ปี : <span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$leave["lf_date1"]?>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br>
      </div>

      <div class="col-xs-12">
        <b>เรื่อง</b>
        <span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         ขออนุญาตลา
         <?
      if($leave[lf_type] == 1 ){echo"ป่วย";}
      elseif($leave[lf_type] == 2 ){echo"กิจส่วนตัว";}
      elseif($leave[lf_type] == 3 ){echo"คลอด";}
      ?>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <br> <br>
         <b>เรียน</b>
         <span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          ผู้อำนวยการสถาบันนวัตกรรมการเรียนรู้
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <br><br>

         ข้าพเจ้า
         <span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <?=$emp["emp_title"]?><?=$emp["emp_name"]?>&nbsp;<?=$emp["emp_lname"]?>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>

         ตำแหน่ง
         <span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <?=$emp["emp_position"]?>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <br>
         สังกัด
          <span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     	 <?=$emp["emp_unit"]?>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </span>
          <br><br>

          <table width="800" >
        <tr>
          <td rowspan="3"  align="left" width="100">ขอลา</td>
          <td><input type="checkbox" <? if($leave["lf_type"] == 1){?> checked="checked" <? }?> />&nbsp;&nbsp;ป่วย</td>
        </tr>
        <tr>
          <td><input type="checkbox" <? if($leave["lf_type"] == 2){?> checked="checked" <? }?> />&nbsp;&nbsp;กิจส่วนตัว&nbsp; เนื่องจาก
          <span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <?=$leave["lf_reason"]?>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
          </td>
        </tr>
        <tr>
          <td><input type="checkbox" <? if($leave["lf_type"] == 3){?> checked="checked" <? }?> />&nbsp;&nbsp;คลอดบุตร</td>
        </tr>
          </table>

          <br>
          ตั้งแต่วันที่
      	<span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	<? echo DateThai($leave["lf_start"]); ?>&nbsp;&nbsp;
          <?

      	if($leave["lf_halfday"]=='2' ){echo "(ครึ่งวันเช้า)";}
          else if ($leave["lf_halfday"]=='3' ){echo "(ครึ่งวันบ่าย)";}
      	?>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        	&nbsp;&nbsp; ถึงวันที่ &nbsp;&nbsp;
         <span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <?  echo DateThai($leave["lf_end"]); ?>&nbsp;&nbsp;
          <?

      	if($leave["lf_halfday2"]=='2' ){echo "(ครึ่งวันเช้า)";}
          else if ($leave["lf_halfday2"]=='3' ){echo "(ครึ่งวันบ่าย)";}
      	?>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
          มีกำหนด
          <span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	<?=$leave["lf_day"]?>
          <?
      	if($leave[lf_holiday] == '1' ){echo"(ลาคร่อมวันเสาร์ - อาทิตย์)";}
      	else if($leave[lf_holiday] == '2' ){echo"(ลาคร่อมวันหยุดนักขัตฤกษ์ 1 วัน)";}
      	else if($leave[lf_holiday] == '3' ){echo"(ลาคร่อมวันหยุดนักขัตฤกษ์ 2 วัน)";}
      	else if($leave[lf_holiday] == '4' ){echo"(ลาคร่อมวันหยุดนักขัตฤกษ์ 3 วัน)";}
      	?>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> วัน
          <br>
          ข้าพเจ้าได้ลา
          &nbsp;&nbsp;&nbsp;&nbsp;
          <input type="checkbox" <? if($leave["lf_last_type"] == 1){?> checked="checked" <? }?> />&nbsp;&nbsp;ป่วย &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="checkbox" <? if($leave["lf_last_type"] == 2){?> checked="checked" <? }?> />&nbsp;&nbsp;กิจส่วนตัว &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="checkbox" <? if($leave["lf_last_type"] == 3){?> checked="checked" <? }?> />&nbsp;&nbsp;คลอดบุตร &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <br>
          ครั้งล่าสุดตั้งแต่วันที่
         <span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     	<? if($leave["lf_last_start"]){$strDate=$leave["lf_last_start"]; echo DateThai($strDate);}else echo "-" ; ?>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
         ถึงวันที่
         <span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <? if($leave["lf_last_end"]){$strDate=$leave["lf_last_end"]; echo DateThai($strDate); }else echo "-" ;?>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
         &nbsp;มีกำหนด
         <span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <? if($leave["lf_last_day"]){echo $leave["lf_last_day"];}else echo "-" ; ?>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>วัน
         <br>
         ในระหว่างลาจะติดต่อข้าพเจ้าได้ที่เบอร์โทรศัพท์
          <span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     	<?=$emp["emp_tel"]?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
         &nbsp;หรือ E-mail <span style='border-bottom:#000 1px dotted'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$emp["emp_mail"]?>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <br><br>

         <div class="col-sm-12" align = "right">
         ขอแสดงความนับถือ
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <br><br>
         (ลงชื่อ)&nbsp;&nbsp;
          <span style='border-bottom:#000 1px dotted'>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         </span>
         <br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         (
          <span style='border-bottom:#000 1px dotted'>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </span>
         )
       </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <!-- Table row -->
     <h2 class="page-header" align = "center" style="font-family: serif;"></h2>
    <div class="row">
      <div class="col-xs-6"  align = "center"><br>
        <strong>สถิติการลาในปีงบประมาณนี้</strong> <br><br>
        <table border="1" cellpadding="0" cellspacing="0">
          <tr >
            <th width="30%" style="text-align:center">ประเภทการลา</th>
            <th width="20%" style="text-align:center">ลามาแล้ว <br>(วันทำการ)</th>
            <th width="20%" style="text-align:center">ลาครั้งนี้ <br>(วันทำการ)</th>
            <th width="20%" style="text-align:center">รวมเป็น <br>(วันทำการ)</th>
          </tr>

          <tbody align = "center">
          <tr>
            <td>ลาป่วย</td>
            <td><? if ($leave["lf_sick1"]){ echo $leave["lf_sick1"]; } else{ echo "-"; } ?></td>
            <td><? if ($leave["lf_sick2"]){ echo $leave["lf_sick2"]; } else{ echo "-"; } ?></td>
            <td><?=$leave["lf_sick1"] + $leave["lf_sick2"]?></td>
          </tr>
          <tr>
            <td>ลากิจ</td>
            <td><? if ($leave["lf_kit1"]){ echo $leave["lf_kit1"]; } else{ echo "-"; } ?></td>
            <td><? if ($leave["lf_kit2"]){ echo $leave["lf_kit2"]; } else{ echo "-"; } ?></td>
            <td><?=$leave["lf_kit1"] + $leave["lf_kit2"]?></td>
          </tr>
          <tr>
            <td>ลาคลอด</td>
            <td><? if ($leave["lf_son1"]){ echo $leave["lf_son1"]; } else{ echo "-"; } ?></td>
            <td><? if ($leave["lf_son2"]){ echo $leave["lf_son2"]; } else{ echo "-"; } ?></td>
            <td><?=$leave["lf_son1"] + $leave["lf_son2"]?></td>
          </tr>
          </tbody>
        </table>

        <br><div align = "center">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            (ลงชื่อ)&nbsp;&nbsp;
            <span style='border-bottom:#000 1px dotted'>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span> ผู้ตรวจสอบ
            <br> <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            ตำแหน่ง &nbsp;&nbsp;
            <span style='border-bottom:#000 1px dotted'>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;
            นักทรัพยากรบุคคล
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;
           </span><br><br>
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           วันที่ &nbsp;&nbsp;
             <span style='border-bottom:#000 1px dotted'>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            /&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            /&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span>
          </div>
      </div>
      <!-- /.col -->
          <div class="col-xs-13" align = "center">
            <br>
             <strong>ความเห็นผู้บังคับบัญชา (ชั้นต้น) </strong>
             <br>
             <span style='border-bottom:#000 1px dotted'>
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             </span><br>
              <span style='border-bottom:#000 1px dotted'>
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             </span><br><br>
             (ลงชื่อ)&nbsp;&nbsp;
              <span style='border-bottom:#000 1px dotted'>
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             </span>
             <br> <br>
             ตำแหน่ง
              <span style='border-bottom:#000 1px dotted'>
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span><br><br>
             วันที่ &nbsp;&nbsp;
              <span style='border-bottom:#000 1px dotted'>
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             /&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             /&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             </span>
            <br> <br>

            <div  class="col-xs-6"  align = "center">
            <strong> คำสั่ง </strong>
            <br>
            <input type="checkbox"  /> อนุญาต
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="checkbox" /> ไม่อนุญาต
            <br>
            <span style='border-bottom:#000 1px dotted'>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span><br>
             <span style='border-bottom:#000 1px dotted'>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span><br><br>
            (ลงชื่อ)&nbsp;
             <span style='border-bottom:#000 1px dotted'>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span>
            <br> <br>
            ตำแหน่ง &nbsp;
             <span style='border-bottom:#000 1px dotted '>
            &nbsp;&nbsp;
            <span>รักษาการแทนผู้อำนวยการสถาบันนวัตกรรมการเรียนรู้ </span>
            &nbsp;&nbsp;
           </span><br>
            วันที่&nbsp;&nbsp;
             <span style='border-bottom:#000 1px dotted'>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            /&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            /&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span>
          </div>
    </div>
    <!-- /.row -->
    <div class="row">
      <!-- accepted payments column
      <div class="col-xs-6">
        <p class="lead">Payment Methods:</p>
        <img src="../../dist/img/credit/visa.png" alt="Visa">
        <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
        <img src="../../dist/img/credit/american-express.png" alt="American Express">
        <img src="../../dist/img/credit/paypal2.png" alt="Paypal">

        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
          Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr
          jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
        </p>
      </div>

      <div class="col-xs-6">
        <p class="lead">Amount Due 2/22/2014</p>
        <div class="table-responsive">
          <table class="table">
            <tr>
              <th style="width:50%">Subtotal:</th>
              <td>$250.30</td>
            </tr>
            <tr>
              <th>Tax (9.3%)</th>
              <td>$10.34</td>
            </tr>
            <tr>
              <th>Shipping:</th>
              <td>$5.80</td>
            </tr>
            <tr>
              <th>Total:</th>
              <td>$265.24</td>
            </tr>
          </table>
        </div>
      </div>  -->
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
